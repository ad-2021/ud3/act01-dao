package es.cipfpbatoi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.dao.ConexionBD;
import es.cipfpbatoi.dao.FacturaDAOImpl;
import es.cipfpbatoi.modelo.Cliente;
import es.cipfpbatoi.modelo.Factura;
import es.cipfpbatoi.modelo.Vendedor;

@TestMethodOrder(OrderAnnotation.class)
class TestFacturaDAO {
	static FacturaDAOImpl capaDao;
	Factura registroVacio = new Factura();
	Cliente cli1 = new Cliente(1, "Matt Design", "C/ Pintor Sorolla, 3");
	Cliente cli2 = new Cliente(3, "John Smith", "Avd. Zum Felde, 2024");
	Vendedor ven = new Vendedor(1, "Carlos Zaltzmann", LocalDate.of(2015, 1, 1), 12000f);
	Factura registroExiste1 = new Factura(1, LocalDate.of(2008, 3, 18), cli1, ven, "Contado");
	Factura registroExiste2 = new Factura(18, LocalDate.of(2011, 11, 02), cli2, ven, "transferencia");
	Factura registroNoExiste = new Factura(10000, LocalDate.of(0, 1, 1), new Cliente(), new Vendedor(), null);
	Factura registroNuevo = new Factura(LocalDate.of(2021, 12, 1), cli1, ven, "tarjeta");
	Factura registroModificarBorrar = new Factura(5000, LocalDate.of(2021, 12, 2), cli2, ven, "Contado");
	static int numRegistrosEsperado = 5000;
	static int autoIncrement = 5000;
	final static String TABLA = "facturas";
	final static String BD = "empresa_ad_test";

	@BeforeAll
	static void setUpBeforeClass() {
		try {
			capaDao = new FacturaDAOImpl();

			ConexionBD.getConexion().createStatement()
					.executeUpdate("delete from " + BD + "." + TABLA + " where id >= " + numRegistrosEsperado);

			if (ConexionBD.getConexion().getMetaData().getDatabaseProductName() == "MariaDB") {
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER TABLE " + BD + "." + TABLA + " AUTO_INCREMENT = " + autoIncrement);
			} else { // PostgreSQL
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER SEQUENCE " + BD + "." + TABLA + "_id_seq RESTART WITH " + autoIncrement);
			}

			ConexionBD.getConexion().createStatement().executeUpdate("insert into " + BD + "." + TABLA
					+ "(fecha, cliente, vendedor, formapago) values ('2021-11-30', 4, 2, 'Contado')");

		} catch (SQLException e) {
			fail("El test falla al preparar el test (instanciando dao: posiblemente falla la conexión a la BD)");
		}
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		capaDao.cerrar();
	}

//	@BeforeEach
//	static void setUp() {
//		try {
//			ConexionBD.getConexion().createStatement().executeUpdate("delete from empresa_ad.clientes where id > 5");
//		} catch (SQLException e) {
//			fail("El test falla en la preparación antes de cada test (preparando tabla clientes)");
//		}
//	}

	@Test
	@Order(1)
	void testFindByPK() {
		try {
			Factura registroObtenido = capaDao.findByPK(registroExiste1.getId());
			Factura registroEsperado = registroExiste1;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroExiste2.getId());
			registroEsperado = registroExiste2;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroNoExiste.getId());
			assertNull(registroObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindAll() {
		try {
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testInsert() {
		try {
			boolean respuestaObtenida = capaDao.insert(registroNuevo);
			assertTrue(respuestaObtenida);
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado + 1, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(3)
	void testInsertGenKey() {
		try {
			Factura registroObtenido = capaDao.insertGenKey(registroNuevo);
			assertNotNull(registroObtenido);
			assertNotEquals(0, registroObtenido.getId());
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(4)
	void testUpdate() {
		try {
			boolean respuestaObtenida = capaDao.update(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.update(registroNoExiste);
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en la actualización por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(5)
	void testDelete() {
		try {
			boolean respuestaObtenida = capaDao.delete(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.delete(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testSize() {
		try {
			int respuestaObtenida = capaDao.size();
			assertEquals(numRegistrosEsperado, respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(6)
	void testExists() {
		try {
			boolean respuestaObtenida = capaDao.exists(registroExiste1.getId());
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.exists(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindByCliente() {
		try {
			int numRegistrosObtenido = capaDao.findByCliente(cli1).size();
			assertEquals(1656, numRegistrosObtenido);

			numRegistrosObtenido = capaDao.findByCliente(cli2).size();
			assertEquals(1681, numRegistrosObtenido);

			numRegistrosObtenido = capaDao.findByCliente(new Cliente(100, null, null)).size();
			assertEquals(0, numRegistrosObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindByVendedor() {
		try {
			int numRegistrosObtenido = capaDao.findByVendedor(ven).size();
			assertEquals(2469, numRegistrosObtenido);

			numRegistrosObtenido = capaDao.findByVendedor(new Vendedor(100, "No existe", null, 0f)).size();
			assertEquals(0, numRegistrosObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

}
