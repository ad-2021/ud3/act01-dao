package es.cipfpbatoi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.dao.ConexionBD;
import es.cipfpbatoi.dao.GrupoDAOImpl;
import es.cipfpbatoi.modelo.Grupo;

@TestMethodOrder(OrderAnnotation.class)
class TestGrupoDAO {
	static GrupoDAOImpl capaDao;
	Grupo registroVacio = new Grupo();
	Grupo registroExiste1 = new Grupo(1, "Hardware");
	Grupo registroExiste2 = new Grupo(3, "Otros");
	Grupo registroNoExiste = new Grupo(100, null);
	Grupo registroNuevo = new Grupo("insert test");
	Grupo registroModificarBorrar = new Grupo(4, "update test");
	static int numRegistrosEsperado = 4;
	static int autoIncrement = 4;
	final static String TABLA = "grupos";
	final static String BD = "empresa_ad_test";

	@BeforeAll
	static void setUpBeforeClass() {
		try {
			capaDao = new GrupoDAOImpl();

			ConexionBD.getConexion().createStatement().executeUpdate("delete from "+BD+"."+TABLA+" where id >= "+numRegistrosEsperado);

			if (ConexionBD.getConexion().getMetaData().getDatabaseProductName() == "MariaDB") {
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER TABLE " + BD + "." + TABLA + " AUTO_INCREMENT = " + autoIncrement);
			} else { // PostgreSQL
				System.out.println("antes");
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER SEQUENCE " + BD + "." + TABLA + "_id_seq RESTART WITH " + autoIncrement);
				System.out.println("despues");
			}

			ConexionBD.getConexion().createStatement().executeUpdate(
					"insert into " + BD + ".grupos(descripcion) values ('descrip test')");

		} catch (SQLException e) {
			fail("El test falla al preparar el test (instanciando dao: posiblemente falla la conexión a la BD)");
		}
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		capaDao.cerrar();
	}

//	@BeforeEach
//	static void setUp() {
//		try {
//			ConexionBD.getConexion().createStatement().executeUpdate("delete from empresa_ad.clientes where id > 5");
//		} catch (SQLException e) {
//			fail("El test falla en la preparación antes de cada test (preparando tabla clientes)");
//		}
//	}

	@Test
	@Order(1)
	void testFindByPK() {
		try {
			Grupo registroObtenido = capaDao.findByPK(registroExiste1.getId());
			Grupo registroEsperado = registroExiste1;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroExiste2.getId());
			registroEsperado = registroExiste2;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroNoExiste.getId());
			assertNull(registroObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindAll() {
		try {
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testInsert() {
		try {
			boolean respuestaObtenida = capaDao.insert(registroNuevo);
			assertTrue(respuestaObtenida);
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado + 1, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(3)
	void testInsertGenKey() {
		try {
			Grupo registroObtenido = capaDao.insertGenKey(registroNuevo);
			assertNotNull(registroObtenido);
			assertNotEquals(0, registroObtenido.getId());
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(4)
	void testUpdate() {
		try {
			boolean respuestaObtenida = capaDao.update(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.update(registroNoExiste);
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en la actualización por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(5)
	void testDelete() {
		try {
			boolean respuestaObtenida = capaDao.delete(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.delete(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testSize() {
		try {
			int respuestaObtenida = capaDao.size();
			assertEquals(numRegistrosEsperado, respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(6)
	void testExists() {
		try {
			boolean respuestaObtenida = capaDao.exists(registroExiste1.getId());
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.exists(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}	
	

}
