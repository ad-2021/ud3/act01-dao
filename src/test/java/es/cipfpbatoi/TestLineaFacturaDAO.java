package es.cipfpbatoi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.dao.ConexionBD;
import es.cipfpbatoi.dao.LineaFacturaDAOImpl;
import es.cipfpbatoi.modelo.Articulo;
import es.cipfpbatoi.modelo.Grupo;
import es.cipfpbatoi.modelo.LineaFactura;

@TestMethodOrder(OrderAnnotation.class)
class TestLineaFacturaDAO {
	static LineaFacturaDAOImpl capaDao;
	Articulo art1 = new Articulo(1, "Monitor 20", 178f, "mon20", new Grupo(1, "Hardware"));
	LineaFactura registroVacio = new LineaFactura();
	LineaFactura registroExiste1 = new LineaFactura(1, 35, art1, 3, 534);
	LineaFactura registroExiste2 = new LineaFactura(1, 1538, art1, 1, 178);
	LineaFactura registroNoExiste = new LineaFactura(100, 1538, art1, 1, 178);
	LineaFactura registroNuevo = new LineaFactura(4, 1, art1, 1, 178);
	LineaFactura registroModificarBorrar = new LineaFactura(3, 1, art1, 5, 5*178);
	static int numRegistrosEsperado = 14001;
	static int numRegistrosEsperadoFactura = 3;
	//static int autoIncrement = x;
	final static String TABLA = "lineas_factura";
	final static String BD = "empresa_ad_test";

	@BeforeAll
	static void setUpBeforeClass() {
		try {
	
			capaDao = new LineaFacturaDAOImpl();
			ConexionBD.getConexion().createStatement()
					.executeUpdate("delete from " + BD + "." + TABLA + " where linea >=3 and factura = 1");

			ConexionBD.getConexion().createStatement().executeUpdate("insert into " + BD + "." + TABLA
					+ "(linea, factura, articulo, cantidad, importe) values (3, 1, 1, 5, 178)");

		} catch (SQLException e) {
			fail("El test falla al preparar el test (instanciando dao: posiblemente falla la conexión a la BD)");
		}
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		capaDao.cerrar();
	}

//	@BeforeEach
//	static void setUp() {
//		try {
//			ConexionBD.getConexion().createStatement().executeUpdate("delete from empresa_ad.clientes where id > 5");
//		} catch (SQLException e) {
//			fail("El test falla en la preparación antes de cada test (preparando tabla clientes)");
//		}
//	}

	@Test
	@Order(1)
	void testFindByPK() {
		try {
			LineaFactura registroObtenido = capaDao.findByPK(registroExiste1.getLinea(), registroExiste1.getFactura());
			LineaFactura registroEsperado = registroExiste1;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroExiste2.getLinea(), registroExiste2.getFactura());
			registroEsperado = registroExiste2;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroNoExiste.getLinea(), registroNoExiste.getFactura());
			assertNull(registroObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindAll() {
		try {
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testInsert() {
		try {
			boolean respuestaObtenida = capaDao.insert(registroNuevo);
			assertTrue(respuestaObtenida);
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado + 1, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(3)
	void testUpdate() {
		try {
			boolean respuestaObtenida = capaDao.update(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.update(registroNoExiste);
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en la actualización por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(4)
	void testDelete() {
		try {
			boolean respuestaObtenida = capaDao.delete(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.delete(registroNoExiste.getLinea(), registroNoExiste.getFactura());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testSize() {
		try {
			int respuestaObtenida = capaDao.size();
			assertEquals(numRegistrosEsperado, respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(6)
	void testExists() {
		try {
			boolean respuestaObtenida = capaDao.exists(registroExiste1.getLinea(), registroExiste1.getFactura();
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.exists(registroNoExiste.getLinea(), registroNoExiste.getFactura());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testFindByFactura() {
		try {
			int numRegistrosObtenido = capaDao.findByFactura(registroExiste1.getFactura()).size();
			assertEquals(numRegistrosEsperadoFactura, numRegistrosObtenido);
			
			numRegistrosObtenido = capaDao.findByFactura(1000).size();
			assertEquals(numRegistrosEsperadoFactura, numRegistrosObtenido);

			numRegistrosObtenido = capaDao.findByFactura(5000).size();
			assertEquals(0, numRegistrosObtenido);
			
		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

}
