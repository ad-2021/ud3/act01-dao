package es.cipfpbatoi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.dao.ConexionBD;
import es.cipfpbatoi.dao.VendedorDAOImpl;
import es.cipfpbatoi.modelo.Vendedor;

@TestMethodOrder(OrderAnnotation.class)
class TestVendedorDAO {
	static VendedorDAOImpl capaDao;
	Vendedor registroVacio = new Vendedor();
	Vendedor registroExiste1 = new Vendedor(1, "Carlos Zaltzmann", LocalDate.of(2015, 1, 1), 12000f);
	Vendedor registroExiste2 = new Vendedor(2, "Juan Fernandez", LocalDate.of(2014, 3, 1), 11500f);
	Vendedor registroNoExiste = new Vendedor(100, "No existe", LocalDate.of(0, 1, 1), 0f);
	Vendedor registroNuevo = new Vendedor("insert nombre test", LocalDate.of(2021, 5, 12), 10000f);
	Vendedor registroModificarBorrar = new Vendedor(3, "update nombre test", LocalDate.of(2021, 5, 12), 10000f);
	static int numRegistrosEsperado = 3;
	static int autoIncrement = 3;
	final static String TABLA = "vendedores";
	final static String BD = "empresa_ad_test";

	@BeforeAll
	static void setUpBeforeClass() {
		try {
			capaDao = new VendedorDAOImpl();

			ConexionBD.getConexion().createStatement()
					.executeUpdate("delete from " + BD + "." + TABLA + " where id >= " + numRegistrosEsperado);

			if (ConexionBD.getConexion().getMetaData().getDatabaseProductName() == "MariaDB") {
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER TABLE " + BD + "." + TABLA + " AUTO_INCREMENT = " + autoIncrement);
			} else { // PostgreSQL
				ConexionBD.getConexion().createStatement()
						.executeUpdate("ALTER SEQUENCE " + BD + "." + TABLA + "_id_seq RESTART WITH " + autoIncrement);
			}

			ConexionBD.getConexion().createStatement().executeUpdate("insert into " + BD
					+ "." + TABLA + "(nombre, fecha_ingreso, salario) values ('nombre test', '2021-05-12', 15000)");

		} catch (SQLException e) {
			fail("El test falla al preparar el test (instanciando dao: posiblemente falla la conexión a la BD)");
		}
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		capaDao.cerrar();
	}

//	@BeforeEach
//	static void setUp() {
//		try {
//			ConexionBD.getConexion().createStatement().executeUpdate("delete from empresa_ad.clientes where id > 5");
//		} catch (SQLException e) {
//			fail("El test falla en la preparación antes de cada test (preparando tabla clientes)");
//		}
//	}

	@Test
	@Order(1)
	void testFindByPK() {
		try {
			Vendedor registroObtenido = capaDao.findByPK(registroExiste1.getId());
			Vendedor registroEsperado = registroExiste1;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroExiste2.getId());
			registroEsperado = registroExiste2;
			assertEquals(registroEsperado, registroObtenido);

			registroObtenido = capaDao.findByPK(registroNoExiste.getId());
			assertNull(registroObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindAll() {
		try {
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(2)
	void testInsert() {
		try {
			boolean respuestaObtenida = capaDao.insert(registroNuevo);
			assertTrue(respuestaObtenida);
			int numRegistrosObtenido = capaDao.findAll().size();
			assertEquals(numRegistrosEsperado + 1, numRegistrosObtenido);
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(3)
	void testInsertGenKey() {
		try {
			Vendedor registroObtenido = capaDao.insertGenKey(registroNuevo);
			assertNotNull(registroObtenido);
			assertNotEquals(0, registroObtenido.getId());
		} catch (SQLException e) {
			fail("El test falla en la inserción por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(4)
	void testUpdate() {
		try {
			boolean respuestaObtenida = capaDao.update(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.update(registroNoExiste);
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en la actualización por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(5)
	void testDelete() {
		try {
			boolean respuestaObtenida = capaDao.delete(registroModificarBorrar);
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.delete(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testSize() {
		try {
			int respuestaObtenida = capaDao.size();
			assertEquals(numRegistrosEsperado, respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(6)
	void testExists() {
		try {
			boolean respuestaObtenida = capaDao.exists(registroExiste1.getId());
			assertTrue(respuestaObtenida);
			respuestaObtenida = capaDao.exists(registroNoExiste.getId());
			assertFalse(respuestaObtenida);
		} catch (SQLException e) {
			fail("El test falla en el borrado por un problema con la BD" + e.getMessage());
		}
	}

	@Test
	@Order(1)
	void testFindExample() {
		try {
			int numRegistrosObtenido = capaDao.findByExample(registroVacio).size();
			assertEquals(numRegistrosEsperado, numRegistrosObtenido);
			numRegistrosObtenido = capaDao.findByExample(registroExiste1).size();
			assertEquals(1, numRegistrosObtenido);

			Vendedor registro = new Vendedor("los", null, 0f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(1, numRegistrosObtenido);

			registro = new Vendedor("a", null, 0f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(2, numRegistrosObtenido);

			registro = new Vendedor("NADA", LocalDate.of(2000, 1, 1), 1f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(0, numRegistrosObtenido);

			registro = new Vendedor("a", LocalDate.of(2000, 1, 1), 1f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(0, numRegistrosObtenido);

			registro = new Vendedor(null, LocalDate.of(2016, 1, 1), 0f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(2, numRegistrosObtenido);

			registro = new Vendedor(null, null, 12000f);
			numRegistrosObtenido = capaDao.findByExample(registro).size();
			assertEquals(2, numRegistrosObtenido);

		} catch (SQLException e) {
			fail("El test falla por un problema con la BD" + e.getMessage());
		}
	}

}
